const jwt = require("jsonwebtoken");
const config = require("config");

module.exports = {
  getDecoded: (req) => {
    const token = req.header("x-auth-token");

    try {
      const decoded = jwt.verify(token, config.get("jwtsecret"));
      return decoded;
    } catch {
      return null;
    }
  },
  checkAdmin: (req) => {
    const decoded = module.exports.getDecoded(req);
    const p = decoded.user.privilege;
    return p.toLowerCase() === "admin";
  },
  checkEC: (req) => {
    const decoded = module.exports.getDecoded(req);
    const p = decoded.user.privilege;
    return p.toLowerCase() === "event creator";
  },
  getUserId: (req) => {
    const decoded = module.exports.getDecoded(req);
    return decoded?.user?.id;
  },
};
