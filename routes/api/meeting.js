const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const nonmember = require("../../middleware/nonmember");
const { check, validationResult } = require("express-validator/check");
const Meeting = require("../../models/Meeting");
const config = require("config");
const { getUserId, checkAdmin } = require("../../middleware/helper/userdata");

// @route   GET api/meeting/
// @desc    get all meeting
router.get("/", async (req, res) => {
  try {
    let meetings = {};
    let total= 0;
    const searchQuery = {
      $or: [
        { event: { $regex: `${req.query.s}`, $options: "i" } },
        // { datestart: { $regex: `${req.query.s}`, $options: "i" } },
        // { dateend: { $regex: `${req.query.s}`, $options: "i" } },
      ],
    }

    if (!req.query.s) {
      total = await Meeting.countDocuments();
      meetings = Meeting.find();
    } else {
      total = await Meeting.find(searchQuery).countDocuments();
      meetings = Meeting.find(searchQuery);
    }

    // const newProm = meetings.then()
    // const total = await newProm.count();
    if (+req.query.limit) {
      meetings = await meetings
        .select("-report")
        .sort({ date: -1 })
        .skip(+req.query.skip || 0)
        .limit(+req.query.limit);
    } else {
      meetings = await meetings.select("-report").sort({ date: -1 });
    }

    if (!meetings && !req.query.s) {
      res.status(400).json({ msg: "no meetings" });
      return;
    }

    const resp = { total: total, data: meetings };
    res.json(resp);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "no meetings" });
      return;
    }
    res.status(500).send("Server error");
  }
});

// @route   GET api/meetings/:mid
// @desc    get specific meeting
router.get("/:mid", async (req, res) => {
  try {
    const meeting = await Meeting.findOne({
      _id: req.params.mid,
    });

    if (!meeting) {
      res.status(400).json({ msg: "no meeting" });
      return;
    }

    res.json(meeting);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "no meeting" });
      return;
    }
    res.status(500).send("Server error");
  }
});

// @route   POST api/meeting
// @desc    Add meeting
// @access  Private
router.post(
  "/",
  [
    nonmember,
    check("event", "event is required").not().isEmpty(),
    check("datestart", "datestart is required").not().isEmpty(),
    check("dateend", "dateend is required").not().isEmpty(),
    check("agenda", "agenda is required").not().isEmpty(),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      event,
      leader,
      location_offline,
      location_online,
      datestart,
      dateend,
      agenda,
      attendees,
      pic,
      attachment,
      status,
    } = req.body;

    const creator = getUserId(req);

    try {
      let meetingFields = {
        event,
        leader,
        location_offline,
        location_online,
        datestart,
        dateend,
        agenda,
        attendees,
        pic,
        attachment,
        creator,
        status,
      };

      // // Create PIC for task data -- never mind!
      // if(pic && pic.length > 0) {
      //   console.log(pic);
      // }

      // Create Meeting
      meeting = new Meeting(meetingFields);

      // Save Meeting
      await meeting.save();

      // return profile
      return res.json(meeting);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   DELETE api/meeting/
// @desc    delete meeting
// @access  Private
router.delete("/:mid", [nonmember], async (req, res) => {
  try {
    const creator = getUserId(req);
    const isAdmin = checkAdmin(req);
    if (!isAdmin) {
      const meeting = await Meeting.findOne({
        _id: req.params.mid,
      });

      if (meeting.creator == creator) {
        meeting.remove();
        res.status(200).json({ msg: "Deleted" });
      } else {
        res.status(404).send({ msg: "This meeting wasnt created by you" });
      }
      return;
    } else {
      await Meeting.findOneAndDelete({ _id: req.params.mid }, (err, item) => {
        console.log(item);
        console.log(err);
        if (!err) res.status(200).json({ msg: "Deleted" });
        else res.status(404).send("Not Found");
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).send("Server error");
  }
});

// @route   PUT api/meeting
// @desc    update meeting
// @access  Private
router.put(
  "/:mid",
  [
    nonmember,
    check("event", "event is required").not().isEmpty(),
    check("datestart", "datestart is required").not().isEmpty(),
    check("dateend", "dateend is required").not().isEmpty(),
    check("agenda", "agenda is required").not().isEmpty(),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      event,
      leader,
      location_offline,
      location_online,
      datestart,
      dateend,
      agenda,
      attendees,
      pic,
      attachment,
      status,
    } = req.body;

    const creator = getUserId(req);

    try {
      let newMeetingData = {
        event,
        leader,
        location_offline,
        location_online,
        datestart,
        dateend,
        agenda,
        attendees,
        pic,
        attachment,
        status,
      };
      
      let meeting = await Meeting.findOne({ _id: req.params.mid });
      if (meeting && (meeting.creator == creator || checkAdmin(req))) {
        if (meeting.report) {
          newMeetingData["report"] = meeting.report;
        }
        meeting = await Meeting.findOneAndUpdate(
          { _id: req.params.mid },
          { $set: newMeetingData },
          { new: true }
        );

        return res.json(meeting);
      }

      // return user
      return res.status(400).json({ msg: "No meeting data or unauthorized" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
