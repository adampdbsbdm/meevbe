const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");
const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");

// @route   GET api/users
// @desc    Test route
// @access  Public
router.get("/", (req, res) => {
  res.send("user route");
});

// @route   POST api/users
// @desc    Test route
// @access  Public
router.post(
  "/",
  // Validation
  [
    check("name", "Name is required").not().isEmpty(), //weird
    check("password", "Password min 6").isLength({ min: 6 }),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, password } = req.body;

    try {
      // See if user exist
      let user = await User.findOne({ name });
      if (user) {
        res.status(400).json({ errors: [{ msg: "User sudah terpakai" }] });
      }

      // Create User
      user = new User({
        name,
        password,
      });

      // Encrypt password
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      // Save user
      await user.save();

      // return jwt
      const payload = {
        user: {
          id: user.id, // user._id
        },
      };
      jwt.sign(
        payload,
        config.get("jwtsecret"),
        { expiresIn: 360000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
