const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const nonmember = require("../../middleware/nonmember");
const { check, validationResult } = require("express-validator/check");
const Meeting = require("../../models/Meeting");
// const Task = require("../../models/Task");
// const config = require("config");
const {
  getUserId,
  checkAdmin,
  checkEC,
} = require("../../middleware/helper/userdata");
var ObjectId = require("mongoose").Types.ObjectId;

// @route   GET api/task/
// @desc    get all task,
router.get("/", [auth], async (req, res) => {
  try {
    const userid = new ObjectId(getUserId(req));
    const userids = getUserId(req);
    const searchQuery = {
      $or: [
        { event: { $regex: `${req.query.s || ""}`, $options: "i" } },
        { "pic.task": { $regex: `${req.query.s || ""}`, $options: "i" } },
      ],
    };

    // Ga relevan, tidak nyambung jika event yg dicari
    // const filterSearchQuery = {
    //   $or: [
    //     {
    //       $regexMatch: {
    //         input: "$$p.task",
    //         regex: `${req.query.s || ""}`,
    //       },
    //     },
    //   ],
    // };
    const filterSearchQuery = {};

    const firstFilterPipeline = checkAdmin(req)
      ? { $match: searchQuery }
      : checkEC(req)
      ? {
          $match: {
            $and: [
              { $or: [{ "pic.aid": userid }, { creator: userids || "" }] },
              searchQuery,
            ],
          },
        }
      : { $match: { "pic.aid": userid, ...searchQuery } };

    const secondFilterPipeline =
      checkAdmin(req) || checkEC(req)
        ? filterSearchQuery
        : { $and: [{ $eq: ["$$p.aid", userid] }, filterSearchQuery] };

    // TBD in pure mongo, slice/limit + filter
    // TODO : this is difficult to read, maybe need to refactor 🤔
    // console.log(userid);
    const result = await Meeting.aggregate([
      firstFilterPipeline,
      {
        $project: {
          _id: 1,
          event: 1,
          creator: 1,
          tasks: {
            $filter: {
              input: "$pic",
              as: "p",
              cond: secondFilterPipeline,
            },
          },
        },
      },
    ]).sort({ date: -1 });
    console.log(result);

    // note : utk EC susah kalau pure mongo
    // ada tambahan kondisinial khusus untuk case EC
    let tasks = [];
    result?.forEach((ev) => {
      ev?.tasks?.forEach((p) => {
        if (!(checkEC(req) && ev.creator != userid) || p.aid == userids) {
          tasks.push({
            mid: ev._id,
            event: ev.event,
            creator: checkAdmin(req) || checkEC(req),
            ...p,
          });
        }
      });
    });
    const total = tasks?.length;

    // console.log("tasks", tasks, total, +req.query.skip);
    if (+req.query.limit) {
      const offset = +req.query.skip || 0;
      // console.log(offset, offset + +req.query.limit);
      tasks = tasks.slice(offset, offset + +req.query.limit);
    }

    if (!tasks && !req.query.s) {
      res.status(400).json({ msg: "no task" });
      return;
    }

    const resp = { total: total, data: tasks };
    res.json(resp);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "no task" });
      return;
    }
    res.status(500).send("Server error");
  }
});

// @route   PUT api/tasks
// @desc    update tasks
// @access  Private
router.put(
  "/:tid",
  [
    auth,
    check("approved", "status is required").not().isEmpty(),
    check("status", "status is required").not().isEmpty(),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { evidence, approved, status } = req.body;

    const creator = getUserId(req);

    try {
      let meeting = await Meeting.findOne({ "pic._id": req.params.tid });
      if (meeting) {
        const newMeetingData = {
          ...meeting.toObject(),
        };

        const appr =
          meeting.creator == creator || checkAdmin(req) ? { approved } : {};

        newMeetingData.pic = newMeetingData?.pic?.map((p) => {
          if (p._id == req.params.tid) {
            return {
              ...p,
              evidence,
              ...appr,
              status,
            };
          } else {
            return p;
          }
        });

        console.log(newMeetingData);
        meeting = await Meeting.findOneAndUpdate(
          { _id: meeting._id },
          { $set: newMeetingData },
          { new: true }
        );

        return res.json(meeting);
      }

      // return user
      return res.status(400).json({ msg: "No tasks data or unauthorized" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
