const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const nonmember = require("../../middleware/nonmember");
const { check, validationResult } = require("express-validator/check");
const Meeting = require("../../models/Meeting");
const config = require("config");
const { getUserId, checkAdmin } = require("../../middleware/helper/userdata");

// @route   GET api/dashboard/
// @desc    get all dashboard
router.get("/", async (req, res) => {
  try {
    let meetings = {};
    // if (+req.query.limit) {
    //   meetings = await Meeting.find()
    //     .sort({ date: 1 })
    //     .skip(+req.query.skip || 0)
    //     .limit(+req.query.limit);
    // } else {
    meetings = await Meeting.find()
      .sort({ date: 1 })
      .select("_id event leader datestart dateend attendees pic");
    // }

    if (!meetings) {
      res.status(400).json({ msg: "no meetings" });
      return;
    }

    const id = getUserId(req);
    let nat = 0,
      npic = 0,
      nlead = 0,
      ntask = 0;
      nmeeting = 0;
    const currentDate = new Date();
    let meetingObj = meetings; //.toObject()
    if (id) {
      meetingObj = meetingObj.map((m) => {
        const at = m.attendees?.filter((a) => a.aid == id).length;
        const pc = m.pic?.filter((p) => p.aid == id).length;
        const islead = m.leader?.id == id;
        // const ts = m.task?.filter((a) => a.aid == id).length; // task tbd
        const ts = 0; // batal, task sama dengan pic

        // Upcoming
        if (m.dateend >= currentDate) {
          nat += at;
          npic += pc;
          nlead = islead ? nlead + 1 : nlead;
          nmeeting += 1;
        }
        // ntask tbd
        return {
          id: m._id,
          title: m.event,
          start: m.datestart,
          end: m.dateend,
          type: islead
            ? "Leader"
            : pc > 0
            ? "PIC"
            : at > 0
            ? "Invitation"
            : ts > 0
            ? "Task"
            : "Meeting",
        };
      });
    }

    res.json({
      summary: { invitation: nat, pic: npic, leader: nlead, task: ntask, meeting: nmeeting },
      events: meetingObj,
    });
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "no meetings" });
      return;
    }
    res.status(500).send("Server error");
  }
});

module.exports = router;
