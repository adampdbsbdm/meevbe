const express = require("express");
const router = express.Router();
const nonmember = require("../../middleware/nonmember");
const { check, validationResult } = require("express-validator/check");
const Meeting = require("../../models/Meeting");
const { getUserId } = require("../../middleware/helper/userdata");

// @route   GET api/report/:mid
// @desc    get specific report
router.get("/:mid", async (req, res) => {
  try {
    const meeting = await Meeting.findOne({
      _id: req.params.mid,
    });

    if (!meeting) {
      res.status(400).json({ msg: "no meeting" });
      return;
    }

    let respon = {}
    if(meeting.report && Object.keys(meeting.report).length > 0) {
      respon = {
        report: meeting.report,
        id: meeting._id,
        // status: meeting.status
      }
    } else {
      respon = {
        report: {
          attendees: meeting.attendees,
        },
        id: meeting._id,
        // status: meeting.status
      }
    }

    res.json(respon);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "no meeting" });
      return;
    }
    res.status(500).send("Server error");
  }
});

// @route   PUT api/report
// @desc    update report
// @access  Private
router.put(
  "/:mid",
  [
    nonmember,
    check("status", "status is required").not().isEmpty(),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      status,
      attendees,
      pic,
      notes,
      attachment,
    } = req.body;
    
    const notulen = getUserId(req);

    try {
      let newMeetingData = {
        status: status,
        report: {
          attendees,
          pic,
          notes,
          attachment,
          notulen
        },
      };

      let meeting = await Meeting.findOne({ _id: req.params.mid });
      // if (meeting && meeting.creator == creator) {
      if (meeting) {
        newMeetingData = {...meeting.toObject(), ...newMeetingData}
        meeting = await Meeting.findOneAndUpdate(
          { _id: req.params.mid },
          { $set: newMeetingData },
          { new: true }
        );

        return res.json(meeting);
      }

      // return user
      return res.status(400).json({ msg: "no meeting or unauthorized" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
