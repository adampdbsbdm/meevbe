const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const admin = require("../../middleware/admin");
const { check, validationResult } = require("express-validator");
const Audience = require("../../models/Audience");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const nonmember = require("../../middleware/nonmember");
const { checkAdmin, getUserId } = require("../../middleware/helper/userdata");

/////////////////////////////////// User ///////////////////////////////////

// @route   GET api/audience
// @desc    Get User
// @access  Private
router.get(
  "/user",
  // Validation
  [nonmember],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let audiences = {};
      let total = 0;
      const searchQuery = {
        $or: [
          { name: { $regex: `${req.query.s}`, $options: "i" } },
          { username: { $regex: `${req.query.s}`, $options: "i" } },
          { privilege: { $regex: `${req.query.s}`, $options: "i" } },
        ],
      };

      if (!req.query.s) {
        total = await Audience.UserAudience.countDocuments();
        audiences = Audience.UserAudience.find();
      } else {
        total = await Audience.UserAudience.find(searchQuery).countDocuments();
        audiences = Audience.UserAudience.find(searchQuery);
      }

      if (+req.query.limit) {
        audiences = await audiences
          .select("-password")
          .sort({ date: -1 })
          .skip(+req.query.skip || 0)
          .limit(+req.query.limit);
      } else {
        audiences = await audiences.select("-password").sort({ date: -1 });
      }

      if (!audiences && !req.query.s) {
        res.status(400).json({ msg: "no audiences" });
        return;
      }

      const resp = { total: total, data: audiences };
      res.json(resp);
    } catch (err) {
      console.error(err.message);
      if (err.kind == "ObjectId") {
        res.status(400).json({ msg: "no audiences" });
        return;
      }
      res.status(500).send("Server error");
    }
  }
);

// @route   POST api/audience
// @desc    Create User
// @access  Public
router.post(
  "/user",
  // Validation
  [
    admin,
    check("username", "Username min 6").isLength({ min: 6 }), //weird
    check("password", "Password min 6").isLength({ min: 6 }),
    check("name", "Name is required").not().isEmpty(),
    check("privilege", "Privilege is required").not().isEmpty(),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { username, name, password, email, contact, privilege } = req.body;

    try {
      // See if user exist
      let user = await Audience.UserAudience.findOne({ username });
      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User sudah terpakai" }] });
      }

      // Create User
      user = new Audience.UserAudience({
        username,
        name,
        password,
        email,
        contact,
        privilege,
      });

      // Encrypt password
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      // Save user
      await user.save();

      // return jwt
      const payload = {
        user: {
          id: user.id,
          username: user.username,
          privilege: user.privilege,
        },
      };
      jwt.sign(
        payload,
        config.get("jwtsecret"),
        { expiresIn: "365d" },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   DELETE api/audience/
// @desc    delete audience user
// @access  Private
router.delete(
  "/user/:id",
  [admin, check("id", "id is required").not().isEmpty()],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      await Audience.UserAudience.findOneAndDelete(
        { _id: req.params.id },
        (err, item) => {
          if (!err) res.status(200).json({ msg: "Deleted" });
          else res.status(404).send("Not Found");
        }
      );
    } catch (err) {
      console.error(err);
      res.status(500).send("Server error");
    }
  }
);

// @route   PUT api/audience
// @desc    update audience
// @access  Private
router.put(
  "/user/:uid",
  [
    admin,
    check("name", "Name is required").not().isEmpty(),
    // check("privilege", "Privilege is required").not().isEmpty(),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, contact, privilege, password } = req.body;

    try {
      const newUserData = {
        name,
        email,
        contact,
        privilege,
      };

      let user = await Audience.UserAudience.findOne({ _id: req.params.uid });
      if (user) {
        if (password) {
          // Encrypt password
          const salt = await bcrypt.genSalt(10);
          newUserData["password"] = await bcrypt.hash(password, salt);
        } else {
          newUserData["password"] = user.password;
        }
  
        user = await Audience.UserAudience.findOneAndUpdate(
          { _id: req.params.uid },
          { $set: newUserData },
          { new: true }
        );

        return res.json(user);
      }

      // return user
      return res.status(400).json({ msg: "no user" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   POST api/audience/change
// @desc    change some user password
// @access  Public
router.put(
  "/user/change/:id",
  // Validation
  [auth, check("password", "password min 6").isLength({ min: 6 })],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { password } = req.body;

    try {
      // See if user exist
      const salt = await bcrypt.genSalt(10);
      const pass = await bcrypt.hash(password, salt);

      let user = await Audience.UserAudience.findOne({ _id: req.params.id });
      const newUserData = { ...user.toObject(), password: pass };
      await Audience.UserAudience.findOneAndUpdate(
        { _id: req.params.id },
        { $set: newUserData },
        { new: true }
      );

      delete newUserData["password"];
      return res.json(newUserData);
    } catch (err) {
      console.error(err.message);
      return res.status(500).send("Server error");
    }
  }
);

/////////////////////////////////// User for current account JWT ///////////////////////////////////

// @route   GET api/audience/user/account
// @desc    Get User detail for current account jwt
// @access  Private
router.get(
  "/useraccount",
  // Validation
  [auth],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = getUserId(req);
      let audiences = await Audience.UserAudience.findOne({ _id: user }).select(
        "-password"
      );

      if (!audiences) {
        res.status(400).json({ msg: "no audiences" });
        return;
      }

      res.json(audiences);
    } catch (err) {
      console.error(err.message);
      if (err.kind == "ObjectId") {
        res.status(400).json({ msg: "no audiences" });
        return;
      }
      res.status(500).send("Server error");
    }
  }
);

// @route   PUT api/audience/user/account
// @desc    update current account jwt
// @access  Private
router.put(
  "/useraccount",
  [auth, check("name", "Name is required").not().isEmpty()],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    
    const { name, email, contact, photo } = req.body;

    try {
      const newUserData = {
        name,
        email,
        contact,
        photo
      };

      const id = getUserId(req);
      let user = await Audience.UserAudience.findOne({ _id: id });
      if (user) {
        user = await Audience.UserAudience.findOneAndUpdate(
          { _id: id },
          { $set: newUserData },
          { new: true }
        ).select("id username privilege contact email name photo");

        const userobj = user.toObject()
        userobj["id"] = userobj["_id"]
        delete userobj["_id"]
        return res.json(userobj);
      }

      // return user
      return res.status(400).json({ msg: "no user" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

/////////////////////////////////// Groups ///////////////////////////////////

// @route   GET api/audience
// @desc    Get Group
// @access  Public
router.get("/group", [nonmember], async (req, res) => {
  // Checking validator error
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    let audiences = {};
    let total = 0;
    const searchQuery = {
      $or: [
        { name: { $regex: `${req.query.s}`, $options: "i" } },
        { description: { $regex: `${req.query.s}`, $options: "i" } },
        { "members.name": { $regex: `${req.query.s}`, $options: "i" } },
        // { "members.username": { $regex: `${req.query.s}`, $options: "i" } }, // danger?
      ],
    };

    if (!req.query.s) {
      total = await Audience.GroupAudience.countDocuments();
      audiences = Audience.GroupAudience.find();
    } else {
      total = await Audience.GroupAudience.find(searchQuery).countDocuments();
      audiences = Audience.GroupAudience.find(searchQuery);
    }

    if (+req.query.limit) {
      audiences = await audiences
        .sort({ date: -1 })
        .skip(+req.query.skip || 0)
        .limit(+req.query.limit);
    } else {
      audiences = await audiences.sort({ date: -1 });
    }

    if (!audiences && !req.query.s) {
      res.status(400).json({ msg: "no audiences" });
      return;
    }

    const resp = { total: total, data: audiences };
    res.json(resp);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "no audiences" });
      return;
    }
    res.status(500).send("Server error");
  }
});

// @route   POST api/audience
// @desc    Create Group
// @access  Public
router.post(
  "/group",
  [admin, check("name", "Name is required").not().isEmpty()],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, description, members } = req.body;

    try {
      // See if group exist
      let group = await Audience.GroupAudience.findOne({ name });
      if (group) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Group sudah terpakai" }] });
      }

      // Create group
      group = new Audience.GroupAudience({
        name,
        description,
        members,
      });

      // Save group
      await group.save();
      res.status(200).json({ msg: "Created" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   DELETE api/audience/
// @desc    delete audience group
// @access  Private
router.delete(
  "/group/:id",
  [admin, check("id", "id is required").not().isEmpty()],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      await Audience.GroupAudience.findOneAndDelete(
        { _id: req.params.id },
        (err, item) => {
          if (!err) res.status(200).json({ msg: "Deleted" });
          else res.status(404).send("Not Found");
        }
      );
    } catch (err) {
      console.error(err);
      res.status(500).send("Server error");
    }
  }
);

// @route   PUT api/audience
// @desc    update external
// @access  Private
router.put(
  "/group/:id",
  [admin, check("name", "name is required").not().isEmpty()],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, description, members } = req.body;

    try {
      let newGroupData = {
        name,
        description,
        members,
      };

      let group = await Audience.GroupAudience.findOne({ _id: req.params.id });
      if (group) {
        group = await Audience.GroupAudience.findOneAndUpdate(
          { _id: req.params.id },
          { $set: newGroupData },
          { new: true }
        );
        return res.json(group);
      }

      // return user
      return res.status(400).json({ msg: "No group data or unauthorized" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

/////////////////////////////////// Externals ///////////////////////////////////
// @route   GET api/audience
// @desc    Get external
// @access  Public
router.get("/external", [nonmember], async (req, res) => {
  // Checking validator error
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    let audiences = {};
    let total = 0;
    const searchQuery = {
      $or: [
        { name: { $regex: `${req.query.s}`, $options: "i" } },
        { information: { $regex: `${req.query.s}`, $options: "i" } },
        { email: { $regex: `${req.query.s}`, $options: "i" } },
        { contact: { $regex: `${req.query.s}`, $options: "i" } },
      ],
    };

    if (!req.query.s) {
      total = await Audience.ExtAudience.countDocuments();
      audiences = Audience.ExtAudience.find();
    } else {
      total = await Audience.ExtAudience.find(searchQuery).countDocuments();
      audiences = Audience.ExtAudience.find(searchQuery);
    }

    if (+req.query.limit) {
      audiences = await audiences
        .sort({ date: -1 })
        .skip(+req.query.skip || 0)
        .limit(+req.query.limit);
    } else {
      audiences = await audiences.sort({ date: -1 });
    }

    if (!audiences && !req.query.s) {
      res.status(400).json({ msg: "no audiences" });
      return;
    }

    const resp = { total: total, data: audiences };
    res.json(resp);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "no audiences" });
      return;
    }
    res.status(500).send("Server error");
  }
});

// @route   POST api/audience
// @desc    Create external
// @access  Public
router.post(
  "/external",
  [admin, check("name", "Name is required").not().isEmpty()],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, information, email, contact } = req.body;

    try {
      // Create ext
      ext = new Audience.ExtAudience({
        name,
        information,
        email,
        contact,
      });

      // Save ext
      await ext.save();
      res.status(200).json({ id: ext.id, msg: "Created" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   DELETE api/audience/
// @desc    delete audience externals
// @access  Private
router.delete(
  "/external/:id",
  [admin, check("id", "id is required").not().isEmpty()],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      await Audience.ExtAudience.findOneAndDelete(
        { _id: req.params.id },
        (err, item) => {
          if (!err) res.status(200).json({ msg: "Deleted" });
          else res.status(404).send("Not Found");
        }
      );
    } catch (err) {
      console.error(err);
      res.status(500).send("Server error");
    }
  }
);

// @route   PUT api/audience
// @desc    update external
// @access  Private
router.put(
  "/external/:id",
  [admin, check("name", "name is required").not().isEmpty()],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, information, contact, email } = req.body;

    try {
      let newExternalData = {
        name,
        information,
        contact,
        email,
      };

      let external = await Audience.ExtAudience.findOne({ _id: req.params.id });
      if (external) {
        external = await Audience.ExtAudience.findOneAndUpdate(
          { _id: req.params.id },
          { $set: newExternalData },
          { new: true }
        );
        return res.json(external);
      }

      // return user
      return res.status(400).json({ msg: "No external data or unauthorized" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
