const express = require("express");
const router = express.Router();
const Meeting = require("../../models/Meeting");

// @route   GET api/search/
// @desc    get all meeting by search
router.get("/", async (req, res) => {
  try {
    let meetings = await Meeting.find({
      $or: [
        { event: { $regex: `${req.query.s}`, $options: "i" } },
        // { datestart: { $regex: `${req.query.s}`, $options: "i" } },
        // { dateend: { $regex: `${req.query.s}`, $options: "i" } },
      ],
    }) 
    const total = await meetings.countDocuments();

    if (+req.query.limit) {
      meetings = await meetings.select("-report")
        .sort({ date: -1 })
        .skip(+req.query.skip || 0)
        .limit(+req.query.limit);
    } else {
      meetings = await meetings.select("-report").sort({ date: -1 });
    }

    const resp = { total: total, data: meetings };
    res.json(resp);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "no meetings" });
      return;
    }
    res.status(500).send("Server error");
  }
});

// // @route   GET api/search
// // @desc    get all search
// router.get("/", async (req, res) => {
//   try {
//     let concepts = {},
//       experience = {};
//     if (req.query.s) {
//       concepts = await Concept.find({
//         $text: { $search: `/${req.query.s}/`, $caseSensitive: false },
//       })
//         .select("link description date")
//         .sort({ date: -1 });
//       experience = await Experience.find({
//         $text: { $search: `/${req.query.s}/`, $caseSensitive: false },
//       })
//         .select("link shortDescription dateProject")
//         .sort({ dateProject: -1, date: -1 });
//     }

//     const results = [
//       ...concepts.map((c) => {
//         let desc = "";
//         for (const d of c.description) {
//           if (d.type.toLowerCase() === "paragraph") {
//             desc = d.content;
//             break;
//           }
//         }

//         return {
//           type: "concept",
//           text: desc,
//           date: c.date,
//           link: c.link,
//         };
//       }),
//       ...experience.map((c) => {
//         return {
//           type: "experience",
//           text: c.shortDescription,
//           date: c.dateProject,
//           link: c.link,
//         };
//       }),
//     ].sort((a, b) => a.date - b.date);
//     // console.log(results);

//     res.json({ count: results.length, data: results });
//   } catch (err) {
//     console.error(err.message);
//     if (err.kind == "ObjectId") {
//       res.status(400).json({ msg: "not found" });
//       return;
//     }
//     res.status(500).send("Server error");
//   }
// });

module.exports = router;
