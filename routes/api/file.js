const express = require("express");
const router = express.Router();
const multer = require("multer");
const auth = require("../../middleware/auth");
const nonmember = require("../../middleware/nonmember");
const { check, validationResult } = require("express-validator/check");
const config = require("config");
const { getUserId, checkAdmin } = require("../../middleware/helper/userdata");

var path = require("path");
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
  },
});
var upload = multer({ storage: storage, limits: { fileSize: 50000000 } });

// @route   POST api/file
// @desc    Add file
// @access  Private
router.post("/", [auth, upload.single("file")], async (req, res) => {
  // Checking validator error
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    return res.json({
      message: "Successfully uploaded files",
      path: req.file.path.replace("uploads\\", ""),
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server error");
  }
});


// Dipakai jika butuh auth untuk download, sementara blm dipakai
router.get('/download', [auth], function(req, res){
  const file = `${__dirname}/${req.query.path}`;
  res.download(file); // Set disposition and send it.
});

module.exports = router;
