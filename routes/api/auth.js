const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const admin = require("../../middleware/admin");
const { check, validationResult } = require("express-validator/check");
const Audience = require("../../models/Audience");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const { getUserId } = require("../../middleware/helper/userdata");

// @route   GET api/auth
// @desc    Test route
// @access  Public
router.get("/", auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select("-password");
    return res.json(user);
  } catch (err) {
    console.error(err.message);
    return res.status(500).send("Server error");
  }
});

// @route   POST api/auth
// @desc    auth user
// @access  Public
router.post(
  "/",
  // Validation
  [
    check("username", "Username kosong").not().isEmpty(),
    check("password", "Password min 6").isLength({ min: 6 }),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { username, password } = req.body;

    try {
      // See if user exist
      let user = await Audience.UserAudience.findOne({ username });
      if (!user) {
        // user non exist
        return res.status(400).json({ errors: [{err:1, msg: "I wonder what happened" }] });
      }

      const isMatch = await bcrypt.compare(password, user.password);

      // return jwt
      if (isMatch) {
        const payload = {
          user: {
            id: user.id,
            username: user.username,
            privilege: user.privilege,
          },
        };
        jwt.sign(
          payload,
          config.get("jwtsecret"),
          { expiresIn: "365d" },
          (err, token) => {
            if (err) throw err;
            res.json({
              token,
              payload: {
                user: {
                  ...payload.user,
                  contact: user.contact,
                  email: user.email,
                  name: user.name,
                  photo: user.photo
                },
              },
            });
          }
        );
      } else {
        // wrong auth
        return res.status(400).json({ errors: [{err:1, msg: "Nice try!!" }] });
      }
    } catch (err) {
      console.error(err.message);
      return res.status(500).send("Server error");
    }
  }
);

// @route   POST api/auth/change
// @desc    auth user
// @access  Public
router.post(
  "/change",
  // Validation
  [
    auth,
    check("oldpassword", "old Password min 6").isLength({ min: 6 }),
    check("newpassword", "new Password min 6").isLength({ min: 6 }),
  ],
  async (req, res) => {
    // Checking validator error
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { oldpassword, newpassword } = req.body;

    try {
      // See if user exist
      
      const creator = getUserId(req);
      let user = await Audience.UserAudience.findOne({ _id: creator });
      if (!user) {
        // user non exist
        return res.status(400).json({ errors: [{ msg: "I wonder what happened" }] });
      }

      const isMatch = await bcrypt.compare(oldpassword, user.password);

      // return jwt
      if (isMatch) {
        const salt = await bcrypt.genSalt(10);
        const pass = await bcrypt.hash(newpassword, salt);
        const newUserData = {...user.toObject(), password: pass}
        usr = await Audience.UserAudience.findOneAndUpdate(
          { _id: creator },
          { $set: newUserData },
          { new: true }
        );

        delete newUserData['password']
        return res.json(newUserData);
      } else {
        // wrong auth
        return res.status(400).json({ errors: [{ err: 1, msg: "Wrong Password" }] });
      }
    } catch (err) {
      console.error(err.message);
      return res.status(500).send("Server error");
    }
  }
);

module.exports = router;
