const express = require("express");
var cors = require("cors");
const app = express();
const connectdb = require("./config/db");
const connectLimiter = require("./config/limiter");

// set http port
const PORT = process.env.PORT || 5000;

// connect db
connectdb();
const limiter = connectLimiter(100, 5);
const search_limiter = connectLimiter(1000, 5);
const email_limiter = connectLimiter(3, 1);

// Init middleware
app.use(cors());
app.use(express.json({ extended: false })); // json parser
app.use(express.static("public"));
app.use(express.static("uploads"));

app.get("/", (req, res) => {
  console.log("masuk ke index");
  res.send("meet berjalan");
});

// Define routes

app.use("/api/auth", require("./routes/api/auth"));
app.use("/api/dashboard", require("./routes/api/dashboard"));
app.use("/api/meeting", require("./routes/api/meeting"));
app.use("/api/report", require("./routes/api/report"));
app.use("/api/file", require("./routes/api/file"));
app.use("/api/audience", require("./routes/api/audience"));
app.use("/api/task", require("./routes/api/task"));
app.use("/api/search", require("./routes/api/search"));

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
