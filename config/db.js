const mongoose = require("mongoose");
const config = require("config");
const db = config.get("mongouri");

const connectdb = async () => {
  try {
    await mongoose.connect(db, {
      // permintaan mongoosenya krn ada yang deprecated
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    console.log("Mongodb connected...");
  } catch (err) {
    console.log(err.message);
    process.exit(1);
  }
};

module.exports = connectdb;
