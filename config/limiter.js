const config = require("config");
const db = config.get("mongouri_limiter");
const user = config.get("user");
const pass = config.get("pass");
var RateLimit = require("express-rate-limit");
var MongoStore = require("rate-limit-mongo");

const connectLimiter = (number_of_request, interval_in_minute) => {
  return new RateLimit({
    store: new MongoStore({
      uri: db,
      user: user,
      password: pass,
      // should match windowMs
      expireTimeMs: interval_in_minute * 60 * 1000,
      errorHandler: console.error.bind(null, "rate-limit-mongo"),
      // see Configuration section for more options and details
    }),
    max: number_of_request,
    // should match expireTimeMs
    windowMs: interval_in_minute * 60 * 1000,
  });
};

module.exports = connectLimiter;
