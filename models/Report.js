const mongoose = require("mongoose");

const ReportSchema = new mongoose.Schema({
  attendees: [
    {
      aid: {
        type: mongoose.ObjectId,
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      kind: {
        type: String,
        required: true,
      },
    },
  ],
  notes: [
    {
      point: {
        type: String,
        required: true,
      },
      notes: {
        type: String,
        required: true,
      }
    },
  ],
  pic: [
    {
      aid: {
        type: mongoose.ObjectId,
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      decision: {
        type: String,
        required: true,
      },
      detail: {
        type: String,
        required: true,
      },
      due: {
        type: Date,
        default: Date.now,
        required: true,
      },
    },
  ],
  attachment: {
    type: String,
  },
  notulen: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

const Report = mongoose.model("report", ReportSchema);

exports.Report = Report;
exports.ReportSchema = ReportSchema;
// module.exports = Report;
