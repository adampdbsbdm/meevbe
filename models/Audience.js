const mongoose = require("mongoose");

const UserAudienceSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
  },
  contact: {
    type: String,
  },
  photo: {
    type: String,
  },
  privilege: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

const GroupAudienceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  members: [
    {
      username: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: true
      }
    },
  ],
  date: {
    type: Date,
    default: Date.now,
  },
});

const ExtAudienceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  information: {
    type: String,
  },
  email: {
    type: String,
  },
  contact: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

const UserAudience = mongoose.model("useraudience", UserAudienceSchema);
const GroupAudience = mongoose.model("groupaudience", GroupAudienceSchema);
const ExtAudience = mongoose.model("extaudience", ExtAudienceSchema);

exports.UserAudience = UserAudience;
exports.GroupAudience = GroupAudience;
exports.ExtAudience = ExtAudience;
exports.UserAudienceSchema = UserAudienceSchema;
exports.GroupAudienceSchema = GroupAudienceSchema;
exports.ExtAudienceSchema = ExtAudienceSchema;
// exports.UserSchema = UserSchema;
