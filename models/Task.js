const mongoose = require("mongoose");

const TaskSchema = new mongoose.Schema({
  aid: {
    type: mongoose.ObjectId,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  task: {
    type: String,
    required: true,
  },
  due: {
    type: Date,
    default: Date.now,
    required: true,
  },
  evidence: {
    type: String,
    default : ""
  },
  approved: {
    type: Boolean,
    default: false
  },
  status: {
    type: String,
    default: "Not Yet"
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

const Task = mongoose.model("task", TaskSchema);

exports.Task = Task;
exports.TaskSchema = TaskSchema;
