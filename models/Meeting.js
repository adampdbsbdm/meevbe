const audience = require("./Audience");
const mongoose = require("mongoose");
const { ReportSchema } = require("./Report");
const { TaskSchema } = require("./Task");

const MeetingSchema = new mongoose.Schema({
  event: {
    type: String,
    required: true,
  },
  leader: {
    id: {
      type: String,
    },
    name: {
      type: String,
    },
  },
  location_offline: {
    type: String,
  },
  location_online: {
    type: String,
  },
  datestart: {
    type: Date,
    default: Date.now,
    required: true,
  },
  dateend: {
    type: Date,
    default: Date.now,
    required: true,
  },
  agenda: {
    type: String,
    required: true,
  },
  attendees: [
    {
      aid: {
        type: mongoose.ObjectId,
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      kind: {
        type: String,
        required: true,
      },
    },
  ],
  pic: [
    {
      // {
      //   aid: {
      //     type: mongoose.ObjectId,
      //     required: true,
      //   },
      //   name: {
      //     type: String,
      //     required: true,
      //   },
      //   task: {
      //     type: String,
      //     required: true,
      //   },
      //   description: {
      //     type: String,
      //     required: true,
      //   },
      // },
      type: TaskSchema,
    },
  ],
  attachment: {
    type: String,
  },
  creator: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  report: {
    type: ReportSchema,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

// MeetingSchema.index(
//   {
//     title: "text",
//     components: "text",
//     goal: "text",
//     "howitworks.content": "text",
//     "description.content": "text",
//   },
//   {
//     name: "SearchIndex",
//   }
// );

const Meeting = mongoose.model("meeting", MeetingSchema);

module.exports = Meeting;
